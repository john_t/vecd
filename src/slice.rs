use std::ops::Range;
use std::borrow::Cow;
use super::VecD;
use crate::range::CouldBeRange;

pub struct SliceD<'a, T> {
    pub(crate) vec: &'a VecD<T>,
    pub(crate) dimensions: Cow<'a, [Range<usize>]>,
}

pub struct SliceDMut<'a, T> {
    pub(crate) vec: &'a mut VecD<T>,
    pub(crate) dimensions: Cow<'a, [Range<usize>]>,
}

impl<'a, T> SliceD<'a, T> {
    /// Indexes this from a `&[Range<usize>]`. Will panic if the range is out of
    /// bounds or if the `&[Range<usize>]` is smaller or bigger than dimensions
    #[must_use]
    pub fn get<C: CouldBeRange<usize>>(
        &'a self,
        idx: &'a [C]
    ) -> SliceD<'a, T> {
        SliceD {
            vec: self.vec,
            dimensions: Cow::Owned(idx.iter().enumerate().map(
                |(n, x)| x.as_range(
                    0,
                    self.dimensions[n].end - self.dimensions[n].start
                )
            ).collect()),
        }
    }

}

impl<'a, T> SliceDMut<'a, T> {
    /// Indexes this from a `&[Range<usize>]`. Will panic if the range is out of
    /// bounds or if the `&[Range<usize>]` is smaller or bigger than dimensions
    #[must_use]
    pub fn get_mut<C: CouldBeRange<usize>>(
        &'a mut self,
        idx: &'a [C]
    ) -> SliceDMut<'a, T> {
        SliceDMut {
            dimensions: Cow::Owned(idx.iter().enumerate().map(
                |(n, x)| x.as_range(
                    0,
                    self.dimensions[n].end - self.dimensions[n].start
                )
            ).collect()),
            vec: self.vec,
        }
    }

}
