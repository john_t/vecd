#![allow(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]

pub mod slice;
pub mod range;
use std::ops::{Index, IndexMut, Range};
use slice::SliceD;
use std::borrow::Cow;

pub struct VecD<T> {
    vec: Vec<T>,
    dimensions: Vec<usize>,
}

pub struct DimensionsError;

impl<T> VecD<T> {
    /// Constructs a new, empty `VecD<T>`.
    ///
    /// The vector will allocate the dimensions, which will involve at most
    /// the size of `usize` * `dimensions`
    #[inline]
    #[must_use = "involves allocations that are not free."]
    pub fn new(dimensions: usize) -> Self {
        Self {
            vec: Vec::new(),
            dimensions: vec![0; dimensions],
        }
    }

    /// Constructs a new, empty `VecD<T>` with the specified capacity.
    ///
    /// The vector will be able to hold exactly `capacity`
    /// elements without reallocating. If the `capacity` is 0, the vector
    /// will not allocate.
    ///
    /// It is important to note that although the returned multidimentional
    /// vector will have a zero *length*.
    ///
    /// # Panics
    ///
    /// Panics if the new capacity exceeds `isize::MAX` bytes.
    #[inline]
    #[must_use = "involves allocations which are not free"]
    pub fn with_capacity(dimensions: usize, capacity: usize) -> Self {
        Self {
            vec: Vec::with_capacity(capacity),
            dimensions: vec![0; dimensions],
        }
    }

    /// Creates a `VecD<T>` directly from the raw components of another `VecD`.
    ///
    /// * `dimensions` should have a product equal to the length of the vec.
    #[must_use]
    pub fn from_raw_parts(vec: Vec<T>, dimensions: Vec<usize>) -> Self {
        Self { vec, dimensions }
    }

    /// Converts this into its raw parts
    #[must_use]
    pub fn into_raw_parts(self) -> (Vec<T>, Vec<usize>) {
        (self.vec, self.dimensions)
    }

    /// Gets the dimensions of this `VecD<T>`
    #[must_use] pub fn get_dimensions(&self) -> usize {
        self.dimensions.len()
    }

    /// Converts this into a slice.
    pub fn as_slice(&self) -> slice::SliceD<'_, T> {
        SliceD {
            vec: self,
            dimensions: {
                let mut dimensions = Vec::new();
                for i in &self.dimensions {
                    dimensions.push(Range{start: 0, end: *i})
                }
                Cow::Owned(dimensions)
            }
        }
    }

    /// Gets the length of the `VecD<T>`. This returns the dimensions
    /// multiplied together.
    pub fn len(&self) -> usize {
        self.dimensions.iter().product()
    }


    /// Gets the dimensions of the vector.
    pub fn dimensions(&self) -> &[usize] {
        &self.dimensions
    }
}

impl<T> AsRef<Vec<T>> for VecD<T> {
    fn as_ref(&self) -> &Vec<T> {
        &self.vec
    }
}

impl<T> AsMut<Vec<T>> for VecD<T> {
    fn as_mut(&mut self) -> &mut Vec<T> {
        &mut self.vec
    }
}

impl<T> Index<&[usize]> for VecD<T> {
    type Output = T;

    /// Indexes this from a `&[usize]`. Will panic if the range is out of
    /// bounds or if the `&[usize]` is smaller or bigger than dimensions
    fn index(&self, idx: &[usize]) -> &Self::Output {
        &self.vec[{
            let mut sum = 0;
            let mut product = 1;
            for (n, idx) in idx.iter().enumerate() {
                sum += idx * product;
                product *= self.dimensions[n];
            }
            sum
        }]
    }
}

impl<T> IndexMut<&[usize]> for VecD<T> {
    /// Indexes this from a `&[usize]`. Will panic if the range is out of
    /// bounds or if the `&[usize]` is smaller or bigger than dimensions
    fn index_mut(&mut self, idx: &[usize]) -> &mut Self::Output {
        &mut self.vec[{
            let mut sum = 0;
            let mut product = 1;
            for (n, idx) in idx.iter().enumerate() {
                sum += idx * product;
                product *= self.dimensions[n];
            }
            sum
        }]
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    pub fn index() {
        let vec = VecD {
            vec: vec![
                (0, 0),
                (1, 0),
                (2, 0),
                (0, 1),
                (1, 1),
                (2, 1),
                (0, 2),
                (1, 2),
                (2, 2),
            ],
            dimensions: vec![3, 3, 3],
        };

        assert_eq!(vec[&[1, 1]], (1, 1));
        assert_eq!(vec[&[2, 0]], (2, 0));
        assert_eq!(vec[&[1, 2]], (1, 2));
    }
}
