use std::ops::{Bound, RangeBounds, Range};

/// This represents any object which *could* be a range. Look at the
/// implementors for more details.
pub trait CouldBeRange<T> {
    fn as_range(&self, start: T, end: T) -> Range<T>;
}

impl<RangeType> CouldBeRange<usize> for RangeType
where
    RangeType: RangeBounds<usize>,
{
    fn as_range(&self, start: usize, end: usize) -> Range<usize> {
        Range {
            start: match self.start_bound() {
                Bound::Included(n) => *n,
                Bound::Excluded(n) => n - 1,
                Bound::Unbounded => start,
            },
            end: match self.end_bound() {
                Bound::Included(n) => n - 1,
                Bound::Excluded(n) => *n,
                Bound::Unbounded => end,
            },
        }
    }
}
